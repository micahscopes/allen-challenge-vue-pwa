import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Part1 from '@/components/Part1'
import Part2 from '@/components/Part2'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/part-1',
      name: 'Part 1',
      component: Part1
    },
    {
      path: '/part-2',
      name: 'Part 2',
      component: Part2
    }
  ]
})

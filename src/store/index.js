import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  mutations: {
    respond (state, response) {
      state[response.id] = response.val
    }
  }
})

export default {
  store,
  install (Vue, options) {
    Vue.prototype.$store = store
  }
}
